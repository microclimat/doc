# Notes on Sensors

Pour selectionner un capteur à ultason, nous avions plusieurs critères : 
- emeteur / récepteur
- pouvant supporter des faibles températures
- un gamme de détection minimal faible (15/20cm)

Nous avons donc comparer plusieurs capteurs :
- https://fr.farnell.com/multicomp/mcusd14a40s09rs/transceiver-40khz-14mm-metal/dp/2362679
- https://fr.farnell.com/multicomp/mcusd18a40s09rs-30c/transceiver-40khz-18mm-metal/dp/2362680
- https://fr.farnell.com/murata/ma40s4r/capteur-ultrasonic-0-2-4m-rx/dp/1777668  (Nous avons choisi celui la car il respecte au mieux les critères)

Comme nous n'avons pas encore fait les tests, nous ne savons pas si il conviendra. Nous sommes confiants mais nous n'en sommes pas sure car ce capteur a une diréctivité de 80°. 
Nous esperons que cela ne faussera pas nos mesures. 